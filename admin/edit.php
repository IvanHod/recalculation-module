<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Iblock\SectionTable;
use Bitrix\Catalog\ExtraTable;
use Trinet\ExtraSectionPrice\Orm\SectionextraTable as SectionExtraTable;

$moduleId = 'trinet.extrasectionprice';
if(!Loader::includeModule("catalog")
	|| !Loader::includeModule($moduleId))
	throw new \Exception(Loc::getMessage("TRINET_EXTRASECTIONPRICE.CHECK_MODULES_INCLUDE"));

if(!$USER->IsAdmin())
	throw new \Exception(Loc::getMessage("TRINET_EXTRASECTIONPRICE.ONLY_ADMIN"));

$iblockId = Option::get($moduleId, "active_catalog");

if(!$iblockId)
	throw new \Exception(Loc::getMessage("TRINET_EXTRASECTIONPRICE.NOT_CHOOSE_CATALOG"));

global $APPLICATION;

Loc::loadMessages(__FILE__);

$request = Context::getCurrent()->getRequest();

$extrasectionId = (int)$request->get('ID');

$tabList = array(
	array(
		'DIV' => 'extraSectionEdit01',
		'TAB' => Loc::getMessage('TRINET_EXTRASECTIONPRICE.EDIT_TAB'),
		'TITLE' => Loc::getMessage('TRINET_EXTRASECTIONPRICE.EDIT_TAB_TITLE')
	)
);

$extraFormID = 'sectionExtraRecord';
$control = new CAdminForm($extraFormID, $tabList);
$control->SetShowSettings(false);
unset($tabList);
$extraFormID .= '_form';

$fields = array();
if (
	check_bitrix_sessid()
	&& $request->isPost()
	&& (string)$request->getPost('Update') == 'Y'
)
{
    $rawData = $request->getPostList();
    if($rawData['SECTION_ID'])
        $fields['SECTION_ID'] = $rawData['SECTION_ID'];
    if($rawData['EXTRA_ID'])
        $fields['EXTRA_ID'] = $rawData['EXTRA_ID'];
    if(isset($rawData['RECALCULATE']))
	    $fields['RECALCULATE'] = true;
	if ($extrasectionId == 0)
		$result = SectionExtraTable::add($fields);
	else
		$result = SectionExtraTable::update($extrasectionId, $fields);
	if (!$result->isSuccess())
	{
		$errors = $result->getErrorMessages();
	}
	else
	{
		if ($extrasectionId == 0)
			$extrasectionId = $result->getId();
	}
	unset($result);

	unset($rawData);

	if (empty($errors))
	{
		if ((string)$request->getPost('apply') != '')
		    LocalRedirect($moduleId . '_edit.php?lang=' . LANGUAGE_ID . '&ID=' . $ruleId . '&' . $control->ActiveTabParam() . GetFilterParams('filter_', false));
		else
			LocalRedirect($moduleId . '_list.php?lang=' . LANGUAGE_ID . GetFilterParams('filter_', false));
	}
}

$sections = array();
$rsSection = SectionTable::getList(array(
	"order" => array("LEFT_MARGIN" => "ASC"),
	"filter" => array("IBLOCK_ID" => $iblockId),
	"select" => array("ID", "IBLOCK_ID", "NAME", "DEPTH_LEVEL")
));
while($arSection = $rsSection->fetch())
{
    $sections[$arSection["ID"]] = str_repeat(".", $arSection["DEPTH_LEVEL"]) . $arSection["NAME"];
}

$extras = array();
$rsExtra = ExtraTable::getList(array(
    "select" => array("ID", "NAME", "PERCENTAGE")
));
while($arExtra = $rsExtra->fetch())
{
	$extras[$arExtra["ID"]] = "[{$arExtra["ID"]}] {$arExtra["NAME"]} {$arExtra["PERCENTAGE"]}%";
}

$APPLICATION->SetTitle(
	$ruleId == 0
		? Loc::getMessage('TRINET_EXTRASECTIONPRICE.TITLE_ADD')
		: Loc::getMessage('TRINET_EXTRASECTIONPRICE.TITLE_EDIT', array('#ID#' => $ruleId))
);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

$contextMenuItems = array(
	array(
		'ICON' => 'btn_list',
		'TEXT' => Loc::getMessage('TRINET_EXTRASECTIONPRICE.BACK'),
		'LINK' => $moduleId . '_list.php?lang='.LANGUAGE_ID.GetFilterParams('filter_')
	)
);

$contextMenuItems[] = array(
	'ICON' => 'btn_delete',
	'TEXT' => Loc::getMessage('TRINET_EXTRASECTIONPRICE.DELETE'),
	'LINK' => "javascript:if (confirm('"
        . CUtil::JSEscape(Loc::getMessage('TRINET_EXTRASECTIONPRICE.DELETE_CONFIRM'))."')) window.location='/bitrix/admin/"
        . $moduleId . "_list.php?lang=" . LANGUAGE_ID . "&ID=" . $extrasectionId . "&action=delete&" . bitrix_sessid_get()."';",
	'WARNING' => 'Y',
);

$contextMenu = new CAdminContextMenu($contextMenuItems);
$contextMenu->Show();
unset($contextMenu, $contextMenuItems);

if (!empty($errors))
{
	$errorMessage = new CAdminMessage(
		array(
			'DETAILS' => implode('<br>', $errors),
			'TYPE' => 'ERROR',
			'MESSAGE' => Loc::getMessage('TRINET_EXTRASECTIONPRICE.ERR_SAVE'),
			'HTML' => true
		)
	);
	echo $errorMessage->Show();
	unset($errorMessage);
}

$arSectionExtra = SectionExtraTable::getById($extrasectionId)->fetch();

$control->BeginPrologContent();
$control->EndPrologContent();
$control->BeginEpilogContent();
echo GetFilterHiddens("filter_");?>
	<input type="hidden" name="Update" value="Y">
	<input type="hidden" name="lang" value="<? echo LANGUAGE_ID; ?>">
	<input type="hidden" name="ID" value="<? echo $extrasectionId; ?>">
<?
if(!$extrasectionId)
    echo '<input type="hidden" name="RECALCULATE" value="Y">';
if (!empty($returnUrl))
{
	?><input type="hidden" name="return_url" value="<? echo htmlspecialcharsbx($returnUrl); ?>"><?
}
echo bitrix_sessid_post();
$control->EndEpilogContent();

$control->Begin(array(
	'FORM_ACTION' => $moduleId . '_edit.php?lang='.LANGUAGE_ID
));
$control->BeginNextFormTab();

$control->AddDropDownField(
	'SECTION_ID',
	Loc::getMessage('TRINET_EXTRASECTIONPRICE.SECTION_ID'),
	true,
	$sections,
	$arSectionExtra['SECTION_ID']
);

$control->AddDropDownField(
	'EXTRA_ID',
	Loc::getMessage('TRINET_EXTRASECTIONPRICE.EXTRA_ID'),
	true,
	$extras,
	$arSectionExtra['EXTRA_ID']
);

if($extrasectionId)
{
	$control->AddCheckBoxField(
		"RECALCULATE",
		Loc::getMessage('TRINET_EXTRASECTIONPRICE.RECALCULATE'),
		false,
		false,
		false
	);
}

$control->Buttons(
	array(
		'disabled' => $readOnly,
		'back_url' => $moduleId . "_list.php?lang=" . LANGUAGE_ID.GetFilterParams('filter_')
	)
);

$control->Show();
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');