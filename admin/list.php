<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Trinet\ExtraSectionPrice\Orm\SectionextraTable;

global $APPLICATION, $DB, $USER;

Loc::loadMessages(__FILE__);

$moduleId = 'trinet.extrasectionprice';
if(!Loader::includeModule("catalog")
	|| !Loader::includeModule($moduleId))
    throw new \Exception(Loc::getMessage("TRINET_EXTRASECTIONPRICE.CHECK_MODULES_INCLUDE"));

if(!$USER->IsAdmin())
	throw new \Exception(Loc::getMessage("TRINET_EXTRASECTIONPRICE.ONLY_ADMIN"));

$sTableID = 'tbl_catalog_extra_section';

$oSort = new CAdminSorting($sTableID, "ID", "asc");

$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	'find_id_start',
	'find_id_end',
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = array();

if (!empty($find_id_start))
	$arFilter['>=ID'] = $find_id_start;
if (!empty($find_id_end))
	$arFilter['<=ID'] = $find_id_end;

if (($arID = $lAdmin->GroupAction()) && !$bReadOnly)
{
	if ($_REQUEST['action_target']=='selected')
	{
		$arID = array();
		$dbResultList = SectionextraTable::getList(array(
            'select' => array('ID'),
            'filter' => $arFilter
        ));
		while ($arResult = $dbResultList->Fetch())
			$arID[] = $arResult['ID'];
	}

	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
			continue;

		switch ($_REQUEST['action'])
		{
			case "delete":
				@set_time_limit(0);
				$DB->StartTransaction();
				$res = SectionextraTable::delete($ID);
				if (!$res->isSuccess())
				{
					$DB->Rollback();

					if ($ex = $res->getErrorMessages())
						$lAdmin->AddGroupError($ex, $ID);
					else
						$lAdmin->AddGroupError(Loc::getMessage("TRINET_EXTRASECTIONPRICE.DELETE_ERROR"), $ID);
				}
				else
				{
					$DB->Commit();
				}
				break;
		}
	}
}


$arHeaders = array(
	array(
		"id" => "ID",
		"content" => "ID",
		"sort" => "ID",
		"default" => true
	),
	array(
		"id" => "SECTION_ID",
		"content" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.SECTION_ID"),
		"default" => true
	),
	array(
		"id" => "EXTRA_ID",
		"content" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.EXTRA_ID"),
		"default" => true
	)
);

$lAdmin->AddHeaders($arHeaders);

$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();

$dbResultList = SectionextraTable::getList(array(
    'order'  => array($by => $order),
    'filter' => $arFilter
));

$dbResultList = new CAdminResult($dbResultList, $sTableID);
$dbResultList->NavStart();

$lAdmin->NavText($dbResultList->GetNavPrint(Loc::getMessage('TRINET_EXTRASECTIONPRICE.PAGE_NAV_TITLE')));

while ($arSectionExtra = $dbResultList->NavNext(true, "f_"))
{
    /* @var $f_ID int id записи * */
	$row =& $lAdmin->AddRow($f_ID, $arSectionExtra);

	$row->AddField("ID", $f_ID);

    $row->AddViewField("SECTION_ID", $f_SECTION_ID);
    $row->AddViewField("EXTRA_ID", $f_EXTRA_ID);

	$arActions = array();
	$arActions[] = array(
        "ICON" => "edit",
        "TEXT" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.CAN_EDIT"),
        "ACTION" => $lAdmin->ActionRedirect("/bitrix/admin/" . $moduleId . "_edit.php?ID=".$f_ID."&lang=".LANGUAGE_ID),
        "DEFAULT" => true
    );

    $arActions[] = array(
        "ICON" => "delete",
        "TEXT" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.CAN_DELETE_ALT"),
        "ACTION"=>"if(confirm('" . Loc::getMessage('TRINET_EXTRASECTIONPRICE.CAN_DELETE_CONF')."')) "
            . $lAdmin->ActionDoGroup($f_ID, "delete")
    );

	$row->AddActions($arActions);
	unset($actions);
}

$lAdmin->AddFooter(
	array(
		array(
			"title" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.LIST_SELECTED"),
			"value" => $dbResultList->SelectedRowsCount()
		),
		array(
			'counter' => true,
			'title' => Loc::getMessage('TRINET_EXTRASECTIONPRICE.LIST_CHECKED'),
			'value' => 0
		),
    )
);

$lAdmin->AddGroupActionTable(
	array(
		"delete" => GetMessage("TRINET_EXTRASECTIONPRICE.LIST_DELETE"),
	)
);

$aContext = array(
	array(
		"TEXT" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.ADD_NEW"),
		"ICON" => "btn_new",
		"LINK" => $moduleId . "_edit.php?lang=" . LANG,
		"TITLE" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.ADD_NEW_ALT")
	),
);
if (!empty($aContext))
    $lAdmin->AddAdminContextMenu($aContext);

$lAdmin->CheckListMode();

$APPLICATION->SetTitle(Loc::getMessage('TRINET_EXTRASECTIONPRICE.PAGE_TITLE'));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$oFilter = new CAdminFilter($sTableID . "_filter");

?>
    <form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
		<?$oFilter->Begin();?>
        <tr>
            <td><? echo "ID" ?>:</td>
            <td>
                <input type="text" name="find_id_start" size="10" value="<?=htmlspecialcharsbx($find_id_start); ?>">
                ...
                <input type="text" name="find_id_end" size="10" value="<?=htmlspecialcharsbx($find_id_end); ?>">
            </td>
        </tr>
		<?
		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(),"form" => "find_form"));
		$oFilter->End();
		?>
    </form>
<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>