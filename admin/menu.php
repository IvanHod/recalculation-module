<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $aMenu
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu = array();

$sModuleName = basename(dirname(dirname(__FILE__)));
if (Loader::includeModule("trinet.extrasectionprice"))
{
	// Проверить права
	$aMenu = array(
		"parent_menu" => "global_menu_store",
		"section" => $sModuleName,
		"sort" => 100,
		"text" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.MENU_TITLE"),
		"title" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.MENU_TITLE_ALT"),
		"url" => "/bitrix/admin/" . $sModuleName . "_list.php?lang=" . LANG,
		"items_id" => $sModuleName . "_items",
	);
}

return $aMenu;