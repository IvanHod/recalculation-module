CREATE TABLE IF NOT EXISTS trinet_extra_log(
  ID INT(11) NOT NULL AUTO_INCREMENT,
  TIMESTAMP_X timestamp not null default current_timestamp,
  USER_ID INT(11) not null REFERENCES b_user(ID),
  COUNT_RECORDS INT(11),
  COUNT_SUCCESS INT(11),
  OPERATION VARCHAR (10),
  PRIMARY KEY(ID)
);

CREATE TABLE IF NOT EXISTS trinet_section_extra(
  ID INT(11) NOT NULL AUTO_INCREMENT,
  TIMESTAMP_X timestamp not null default current_timestamp,
  SECTION_ID INT(11) not null REFERENCES b_iblock_section(ID),
  OLD_EXTRA_ID INT(11) REFERENCES b_catalog_extra(ID),
  EXTRA_ID INT(11) not null REFERENCES b_catalog_extra(ID),
  PRIMARY KEY(ID)
);