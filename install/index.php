<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class trinet_extrasectionprice extends CModule
{
	/**
	 * @return string
	 */
	public static function getModuleId()
	{
		return basename(dirname(__DIR__));
	}

	public function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__) . "/version.php");
		$this->MODULE_ID = self::getModuleId();
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("TRINET_EXTRAPRICE_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("TRINET_EXTRAPRICE_MODULE_DESC");
	}

	public function doInstall()
	{
		global $APPLICATION;
		try
		{
			$this->installDb();
			$this->InstallFiles();
			ModuleManager::registerModule($this->MODULE_ID);
		}
		catch (\Exception $exp)
		{
			$APPLICATION->ThrowException($exp->getMessage());
			return false;
		}
		return true;
	}

	public function installDb()
	{
		global $DB, $DBType;
		$errors = $DB->RunSQLBatch(__DIR__. "/db/" . strtolower($DBType) . "/install.sql");
		if ($errors)
		{
			throw new \Exception(implode("<br>", $errors));
		}
		return true;
	}

	public function doUninstall()
	{
		global $APPLICATION;
		try
		{
			$this->unInstallDb();
			$this->UnInstallFiles();
			ModuleManager::unRegisterModule($this->MODULE_ID);
		}
		catch (\Exception $e)
		{
			$APPLICATION->ThrowException($e->getMessage());
			return false;
		}
		return true;
	}

	public function unInstallDb()
	{
		global $DB, $DBType;
		$errors = $DB->RunSQLBatch(__DIR__. "/db/" . strtolower($DBType) . "/uninstall.sql");
		if ($errors)
		{
			throw new \Exception(implode("<br>", $errors));
		}
		return true;
	}

	public function InstallFiles($arParams = array())
	{
		$documentRoot = Application::getDocumentRoot();
		if (is_dir($sAdminPath = $documentRoot . '/local/modules/'.self::getModuleId().'/admin'))
		{
			if ($dir = opendir($sAdminPath))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || $item == 'menu.php')
					{
						continue;
					}
					$sFileContent = "<?require \"$documentRoot/local/modules/" . self::getModuleId() . "/admin/$item\";?>";
					file_put_contents($documentRoot . '/bitrix/admin/'.self::getModuleId().'_' . $item, $sFileContent);
				}
				closedir($dir);
			}
		}

		return true;
	}

	public function UnInstallFiles()
	{
		$documentRoot = Application::getDocumentRoot();
		if (is_dir($sAdminPath = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/'.self::getModuleId().'/admin'))
		{
			if ($dir = opendir($sAdminPath))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || $item == 'menu.php')
					{
						continue;
					}
					unlink($documentRoot . '/bitrix/admin/' . self::getModuleId() . '_' . $item);
				}
				closedir($dir);
			}
		}

		return true;
	}
}