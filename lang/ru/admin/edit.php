<?
$MESS["TRINET_EXTRASECTIONPRICE.CHECK_MODULES_INCLUDE"] = "Проверьте подключение модулей";
$MESS["TRINET_EXTRASECTIONPRICE.EDIT_TAB"] = "Редактирование";
$MESS["TRINET_EXTRASECTIONPRICE.EDIT_TAB_LABEL"] = "Редактирование";
$MESS["TRINET_EXTRASECTIONPRICE.TITLE_ADD"] = "Добавление наценки для раздела";
$MESS["TRINET_EXTRASECTIONPRICE.TITLE_EDIT"] = "Редактирование наценки #ID# для раздела";
$MESS["TRINET_EXTRASECTIONPRICE.BACK"] = "Назад";
$MESS["TRINET_EXTRASECTIONPRICE.ADD_NEW"] = "Создать новую зависимость";
$MESS["TRINET_EXTRASECTIONPRICE.DELETE"] = "Удалить зависимость";
$MESS["TRINET_EXTRASECTIONPRICE.SECTION_ID"] = "Выберете раздел";
$MESS["TRINET_EXTRASECTIONPRICE.EXTRA_ID"] = "Выберете наценку";
$MESS["TRINET_EXTRASECTIONPRICE.RECALCULATE"] = "Перерасчитать";
$MESS["TRINET_EXTRASECTIONPRICE.ONLY_ADMIN"] = "Только администратор имеет доступ к данной странице";
$MESS["TRINET_EXTRASECTIONPRICE.NOT_CHOOSE_CATALOG"] = "Выберите каталог в настройках модуля";