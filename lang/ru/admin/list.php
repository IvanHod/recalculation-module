<?
$MESS["TRINET_EXTRASECTIONPRICE.CHECK_MODULES_INCLUDE"] = "Проверьте подключение модулей";
$MESS["TRINET_EXTRASECTIONPRICE.ADD_NEW"] = "Добавить зависимость";
$MESS["TRINET_EXTRASECTIONPRICE.ADD_NEW_ALT"] = "Добавить зависимость раздела каталога и наценки";
$MESS["TRINET_EXTRASECTIONPRICE.SECTION_ID"] = "Раздел";
$MESS["TRINET_EXTRASECTIONPRICE.EXTRA_ID"] = "Наценка";
$MESS["TRINET_EXTRASECTIONPRICE.PAGE_NAV_TITLE"] = "Наценки";
$MESS["TRINET_EXTRASECTIONPRICE.PAGE_TITLE"] = "Наценки для разделов";
$MESS["TRINET_EXTRASECTIONPRICE.CAN_DELETE_CONF"] = "При удалении наценки, для товаров подраздела вернется прежняя базовая цена";
$MESS["TRINET_EXTRASECTIONPRICE.CAN_DELETE_ALT"] = "Удалить наценку для раздела";
$MESS["TRINET_EXTRASECTIONPRICE.CAN_EDIT"] = "Изменить наценку для раздела";
$MESS["TRINET_EXTRASECTIONPRICE.LIST_DELETE"] = "Удалить несколько записей";
$MESS["TRINET_EXTRASECTIONPRICE.DELETE_ERROR"] = "Ошибка удаление записей";
$MESS["TRINET_EXTRASECTIONPRICE.LIST_SELECTED"] = "Список выбранных записей";
$MESS["TRINET_EXTRASECTIONPRICE.LIST_CHECKED"] = "Отмечено";
$MESS["TRINET_EXTRASECTIONPRICE.ONLY_ADMIN"] = "Только администратор имеет доступ к данной странице";