<?namespace Trinet\Extrasectionprice\Orm;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Config\Option;
use Bitrix\Iblock\SectionTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Catalog\GroupTable;
use Bitrix\Catalog\PriceTable;
use Bitrix\Catalog\ExtraTable;
use Bitrix\Main\Localization\Loc;
use Trinet\ExtraSectionPrice\Orm\LogTable;

Loc::loadMessages(__FILE__);

class SectionextraTable extends DataManager
{

	private static $operaion = null;

	public static function getTableName()
	{
		return "trinet_section_extra";
	}

	public static function getMap()
	{
		return array(
			new IntegerField("ID", array(
				"primary" => true,
			)),
			new DateTimeField("TIMESTAMP_X"),
			new IntegerField("SECTION_ID"),
			new IntegerField("EXTRA_ID"),
			new IntegerField("OLD_EXTRA_ID"),
			new ReferenceField(
				'SECTION',
				'\Bitrix\Iblock\SectionTable',
				array('=this.SECTION_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
			new ReferenceField(
				'EXTRA',
				'\Bitrix\Catalog\ExtraTable',
				array('=this.EXTRA_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
			new ReferenceField(
				'OLD_EXTRA',
				'\Bitrix\Catalog\ExtraTable',
				array('=this.OLD_EXTRA_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
		);
	}

	public static function add(array $data)
	{
		$extraSection = static::getList(array(
			'filter' => array('=SECTION_ID' => $data['SECTION_ID'])
		))->fetch();
		if($extraSection)
			throw new \Exception(Loc::getMessage('TRINET_EXTRASECTIONPRICE.RECORD_EXIST'));
		static::$operaion = 'add';
		$recalculate = isset($data['RECALCULATE']) ? true : false;
		unset($data['RECALCULATE']);

		$data['OLD_EXTRA_ID'] = $data['EXTRA_ID'];
		$result = parent::add($data);
		if($recalculate)
			static::updateElements($data['SECTION_ID'], static::getPercentage($data['EXTRA_ID']));
		return $result;
	}

	public static function update($primary, array $data)
	{
		static::$operaion = 'update';
		$recalculate = isset($data['RECALCULATE']) ? true : false;
		unset($data['RECALCULATE']);

		$oldExtraId = false;
		$arOldExtra = static::getList(array('filter' => array('ID' => $primary)))->fetch();
		if($arOldExtra)
			$oldExtraId = $arOldExtra['OLD_EXTRA_ID'];

		if($recalculate)
			$data['OLD_EXTRA_ID'] = $arOldExtra['EXTRA_ID'];

		$result = parent::update($primary, $data);
		if($recalculate)
			static::updateElements($data['SECTION_ID'], static::getPercentage($data['EXTRA_ID']), static::getPercentage($oldExtraId));
		return $result;
	}

	public static function delete($primary)
	{
		static::$operaion = 'delete';
		$arOldExtra = static::getList(array('filter' => array('ID' => $primary)))->fetch();

		static::updateElements($arOldExtra['SECTION_ID'], 0, static::getPercentage($arOldExtra['EXTRA_ID']));

		return parent::delete($primary);
	}

	/**
	 * @param $sectionId int id раздела
	 * @param $percentage int наценка
	 * @param int $oldPercentage старая наценка
	 */
	public static function updateElements($sectionId, $percentage, $oldPercentage = 0)
	{
		$iblockId = Option::get('trinet.extrasectionprice', 'active_catalog');
		$group = GroupTable::getList(array(
			'select' => array('ID'),
			'filter' => array('=BASE'=>'Y')
		))->fetch();
		$basePriceId = (!empty($group) ? (int)$group['ID'] : 0);
		unset($group);

		$offersList = static::getElements($iblockId, $sectionId);

		$successRecords = 0;
		$errorRecords = 0;
		foreach ($offersList as $arElement)
		{
			foreach ($arElement as $arOffer)
			{
				$rsPrices = PriceTable::getList(array(
					'filter' => array('=PRODUCT_ID' => $arOffer['ID'])
				));
				if ($arPrice = $rsPrices->Fetch())
				{
					if($oldPercentage)
						$arPrice['PRICE'] = $arPrice['PRICE'] / (1 + $oldPercentage/100);
					$res = \CPrice::Update($arPrice['ID'], array(
						'PRODUCT_ID' => $arOffer['ID'],
						'CATALOG_GROUP_ID' => $basePriceId,
						'PRICE' => $arPrice['PRICE'] * (1 + $percentage/100)
					));
					if($res)
						$successRecords++;
					else
						$errorRecords++;
				}
			}
		}
		LogTable::add(array(
			'OPERATION' => static::$operaion,
			'COUNT_RECORDS' => $successRecords,
			'COUNT_SUCCESS' => $successRecords + $errorRecords
		));
	}

	/**
	 * @param $extraId int id наценки
	 * @return float|int процент наценки
	 */
	public static function getPercentage($extraId)
	{
		$percentage = 0;
		if($arExtra = ExtraTable::getList(array('filter' => array('=ID' => $extraId)))->fetch())
		{
			$percentage = floatval($arExtra['PERCENTAGE']);
		}
		return $percentage;
	}

	/**
	 * @param $iblockId int id инфоблока
	 * @param $sectionId int id главного раздела
	 * @return int[] Список id торговых предложений
	 */
	public static function getElements($iblockId, $sectionId)
	{
		$generalSection = array();
		$rsSection = SectionTable::getList(array(
			'filter' => array('=IBLOCK_ID' => $iblockId, '=ID' => $sectionId),
			'select' => array('IBLOCK_ID', 'ID', 'LEFT_MARGIN', 'RIGHT_MARGIN')
		));
		if($arSection = $rsSection->fetch())
			$generalSection = $arSection;
		$rsSection = SectionTable::getList(array(
			'filter' => array(
				'=IBLOCK_ID' => $iblockId,
				'>=LEFT_MARGIN' => $generalSection['LEFT_MARGIN'],
				'<=RIGHT_MARGIN' => $generalSection['RIGHT_MARGIN']),
			'select' => array('IBLOCK_ID', 'ID')
		));

		$sections = array();
		while ($arSection = $rsSection->fetch())
		{
			$sections[] = $arSection['ID'];
		}

		$elementIds = array();
		$rsElement = ElementTable::getList(array(
			'filter' => array('IBLOCK_ID' => $iblockId, 'IBLOCK_SECTION_ID' => $sections),
			'select' => array('IBLOCK_ID', 'ID')
		));
		while ($arElement = $rsElement->fetch())
		{
			$elementIds[] = $arElement['ID'];
		}

		$offersList = \CCatalogSku::getOffersList($elementIds, $iblockId);

		return array_merge($elementIds, $offersList);
	}
}