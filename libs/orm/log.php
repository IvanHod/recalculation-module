<?namespace Trinet\ExtraSectionPrice\Orm;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Entity\BooleanField;
use Bitrix\Main\Type\DateTime;

class LogTable extends DataManager
{
	public static function getTableName()
	{
		return "trinet_extra_log";
	}

	public static function getMap()
	{
		return array(
			new IntegerField("ID", array(
				"primary" => true,
			)),
			new IntegerField("USER_ID"),
			new DateTimeField("TIMESTAMP_X"),
			new IntegerField("COUNT_RECORDS"),
			new BooleanField('IS_SUCCESS', array(
				'values' => array('N', 'Y')
			)),
			new ReferenceField(
				'USER',
				'\Bitrix\Security\Mfa\UserTable',
				array('=this.USER_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
		);
	}

	public static function add(array $data)
	{
		global $USER;
		if(!$data['USER_ID'])
		{
			$data['USER_ID'] = $USER->GetID();
		}
		return parent::add($data);
	}

	/**
	 * @return \Bitrix\Main\DB\Result
	 */
	public static function deleteAll()
	{
		$entity = static::getEntity();
		$connection = $entity->getConnection();
		$tableName = $entity->getDBTableName();
		$sql = "DELETE FROM $tableName";
		return $connection->query($sql);
	}
}