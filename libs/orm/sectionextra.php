<?namespace Trinet\ExtraSectionPrice\Orm;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\ReferenceField;

class SectionextraTable extends DataManager
{
	public static function getTableName()
	{
		return "trinet_section_extra";
	}

	public static function getMap()
	{
		return array(
			new IntegerField("ID", array(
				"primary" => true,
			)),
			new DateTimeField("TIMESTAMP_X"),
			new IntegerField("SECTION_ID"),
			new IntegerField("EXTRA_ID"),
			new ReferenceField(
				'SECTION',
				'\Bitrix\Iblock\SectionTable',
				array('=this.SECTION_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
			new ReferenceField(
				'EXTRA',
				'\Bitrix\Catalog\ExtraTable',
				array('=this.EXTRA_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
		);
	}
}