<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * @var string $mid module id from GET
 */
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Catalog\CatalogIblockTable;

Loc::loadMessages(__FILE__);

global $APPLICATION, $USER;

$module_id = "trinet.extrasectionprice";

Loader::includeModule("catalog");
Loader::includeModule($module_id);

$catalogs = array();
$rsCatalog = CatalogIblockTable::getList(array(
	'select' => array('IBLOCK_ID', 'NAME' => 'IBLOCK.NAME')
));
while($arCatalog = $rsCatalog->fetch())
{
	$catalogs[$arCatalog["IBLOCK_ID"]] = "[{$arCatalog['IBLOCK_ID']}] {$arCatalog['NAME']}";
}

$options = array(
	"general" => array(
		Loc::getMessage("TRINET_EXTRASECTIONPRICE.ACTIVE_CATALOG"),
		array(
            "active_catalog",
            Loc::getMessage("TRINET_EXTRASECTIONPRICE.ACTIVE_CATALOG"),
            "",
            array("selectbox", $catalogs)
        ),
	),
);

$tabs = array(
	array(
		"DIV" => "general",
		"TAB" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.TAB_GENERAL"),
		"TITLE" => Loc::getMessage("TRINET_EXTRASECTIONPRICE.TAB_GENERAL")
	),
);

if ($USER->IsAdmin())
{
	if (check_bitrix_sessid() && strlen($_POST["save"]) > 0)
	{
		__AdmSettingsSaveOptions($module_id, $options["general"]);
		LocalRedirect($APPLICATION->GetCurPageParam());
	}
}

$tabControl = new CAdminTabControl("tabControl", $tabs);
$tabControl->Begin();
?>
<form method="POST"
      action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($mid) ?>&lang=<?= LANGUAGE_ID ?>">
	<? $tabControl->BeginNextTab(); ?>
	<? __AdmSettingsDrawList($module_id, $options["general"]); ?>
	<? $tabControl->Buttons(array("btnApply" => false, "btnCancel" => false, "btnSaveAndAdd" => false)); ?>
	<?= bitrix_sessid_post(); ?>
	<? $tabControl->End(); ?>
</form>